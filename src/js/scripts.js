window.addEventListener("DOMContentLoaded", start);

const settings = {};

async function start() {
  let orderData = await getJson();
  setSplashImg(orderData);
  createArticles(orderData);
  createPageButtons(orderData.articles.length);
  createSponsors(orderData);
  document.querySelector("#search_input").addEventListener("input", function () {
    search(orderData);
  });
}

async function getJson() {
  const url = "../content.json";
  const response = await fetch(url);
  const jsonData = await response.json();

  console.log(jsonData);
  return jsonData;
}

function setSplashImg(orderData) {
  let i = Math.floor(Math.random() * 15);
  document.querySelector("img").src = orderData.articles[i].img;
}

function createArticles(orderData) {
  const container = document.querySelector("#article_section_col");
  const template = document.querySelector("#article_template");

  orderData.articles.forEach((article, index) => {
    let clone = template.content.cloneNode(true);
    /* console.log(article); */
    clone.querySelector("img").src = article.img;
    clone.querySelector("h3").textContent = article.headline;
    clone.querySelector("p").textContent = article.text.substring(0, 200) + "...";
    clone.querySelector("button").addEventListener("click", function () {
      openArticle(index);
    });
    container.append(clone);
  });
}

function createPageButtons(length) {
  let l = 0;
  for (let i = 0; i < length; i++) {
    if (i === 0) {
      createButton(l);
      l++;
    } else if (i % 3 === 0) {
      createButton(l);
      l++;
    }
  }
}

function createButton(index) {
  let button = document.createElement("button");
  button.textContent = index + 1;
  button.addEventListener("click", function () {
    pageButtonPressed(index);
  });
  document.querySelector("#article_section_col").append(button);
  pageButtonPressed(0);
}

function pageButtonPressed(i) {
  articles = document.querySelectorAll("#article_section_col > article");
  articles.forEach((article) => {
    article.style.display = "none";
  });
  articles[i * 3].style.display = "flex";
  articles[i * 3 + 1].style.display = "flex";
  articles[i * 3 + 2].style.display = "flex";
}

function openArticle(index) {
  this.window.open(`./article.html?${index}`, "_self");
}

function createSponsors(orderData) {
  const container = document.querySelector("#sponsors");
  orderData.sponsors.forEach((sponsor) => {
    let img = document.createElement("img");
    img.src = sponsor.img;
    container.append(img);
  });
}

function search(orderData) {
  searchString = document.querySelector("#search_input").value;
  /*   console.log("search"); */
  document.querySelector("#splash_section").style.display = "none";

  if (searchString !== undefined && searchString !== "") {
    const searchResult = orderData.articles.filter((article) => article.text.toLowerCase().includes(searchString) || article.headline.toLowerCase().includes(searchString));
    console.log(searchResult);
    displaySearchResult(searchResult);
  } else {
    resetSearch();
  }
}

function displaySearchResult(searchResult) {
  console.log(searchResult);
  articles = document.querySelectorAll("#article_section_col > article");
  articles.forEach((article) => {
    article.style.display = "none";
    searchResult.forEach((result) => {
      if (result.headline === article.querySelector("h3").textContent) {
        article.style.display = "flex";
      }
    });
  });

  /*   articles[i * 3].style.display = "flex";
  articles[i * 3 + 1].style.display = "flex";
  articles[i * 3 + 2].style.display = "flex"; */
}

function resetSearch() {
  document.querySelector("#splash_section").style.display = "block";
  pageButtonPressed(0);
}
